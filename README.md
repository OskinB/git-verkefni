# Tests

This is an example project of how you can make tests for a simple website about simple things

## Getting Started

To start contributing to this project you should fork it to your own GitLab repository. Then you can clone it to your local repository on your machine

### Prerequisites

You will need to have Node.js on your machine and this document will assume that you have yarn (npm should be ok too). To install yarn do:

```npm install -g yarn```

you will also need to have jest (that is the testing framework: https://jestjs.io/docs/en/getting-started). To install it just run the yarn command in the terminal in your project directory with no arguments like so:

```
 yarn
```

### Opening the page in your browser

Once you fork this library to your own GitLab a CI process will start runing and after it has passed you should be able to run the page under settings > pages

### how to contribute to the project

If you want to add something to the project you will have to edit the main.js file. You can add a link to a image and write a title to the arrays. It is very important that the title is written with first letter uppercase and it should end with a dot, like so: The room.

Add the imgage link:
```
var img = [
    "https://gq-images.condecdn.net/image/qG0vGeYLb8R/crop/1020/f/Room-GQ-31Mar17_rex_b.jpg"  
]

```
Add a title to the movie, remember the first letter is uppercase and the dot in the end.
```
var getTitle = function(n){
    var title = [
        "The room."
    ]
    return title[n];
}

```

Just remember to add a comma after the previous line.

This is it. If you have added your title and image link to the correct places you will have made a successful contribution to the project

## Running the tests

Before you commit your code and do a merge request you should test your code by typing

```yarn test```

into your terminal. If everything is successful you should see something like:

```
Test Suites: 3 passed, 3 total
Tests:       6 passed, 6 total
Snapshots:   0 total
Time:        1.089s
Ran all test suites.
Done in 1.76s.
```


